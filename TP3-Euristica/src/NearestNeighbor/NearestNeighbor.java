package NearestNeighbor; 

public class NearestNeighbor {
 
    private int[] path; /** Armazena o menor caminho percorrido possível. */
    private int[] weight; /** Armazena o peso do menor caminho percorrido possível. */
    private int n; /** Armazena o tamanho da matriz. */

    public NearestNeighbor(final int[][] distance_matrix, final int starting_city) {
        this.n = distance_matrix[0].length;
        this.path = new int[distance_matrix[0].length];
        this.weight = new int[distance_matrix[0].length];

        /** Inicializa a matriz que conterá o caminho minímo com o "valor" máximo,
         uma vez que o problema utiliza-se de comparações entre tamanhos para
         calcular o menor caminho possível. */
        for (int j = 0; j < path.length; j++){
            this.path[j] = Integer.MAX_VALUE;
            this.weight[j] = 0;
        }
        
        /** Define a cidade inicial, escolhida pelo usuário. */
        this.path[0] = starting_city;
        int current_city = starting_city;
        
        int i = 1;
        /** Enquanto existirem cidades a serem visitadas pelo caixeiro. */
        while (i < this.path.length) {
            /** Define a próxima cidade a ser vizitada. */
            int next_city = findMinimumWay(distance_matrix[current_city]);
            /** Se a cidade ainda não foi visitada. */
            if (next_city != -1) {                
                /** Adiciona a cidade ao caminho. */
                this.path[i] = next_city;                
                this.weight[i] = distance_matrix[current_city][next_city];
                /** Atualiza a cidade atual e o contador. */
                current_city = next_city;                
                i++;
            }
        }
    }

    private int findMinimumWay(int[] minimum_way) {
        /** Define o menor custo para o caminho passado como paramêtro. */
        int next_city = -1;
        int i = 0;
        int minimum_value = Integer.MAX_VALUE;

        /** Enquanto existirem cidades a serem visitadas pelo caixeiro. */
        while (i < minimum_way.length) {
            /** Se a cidade ainda não foi visitada e seu custo é menor que o custo
            definido pela variável minimun_value, minimun_value recebe este novo
            custo, e, o caminho é iterado para apróxima cidade. */
            if (!isCityInPath(this.path, i) && minimum_way[i] < minimum_value) {
                minimum_value = minimum_way[i];
                next_city = i;
            }
            i++;
        }
        return next_city;
    }

    /** Retorna o caminho minimo percorrido pelo caixeiro. */
    public int[] getPath() {
        return this.path;
    }

    /** Retorna os pesos do caminho minimo percorrido pelo caixeiro. */
    public int[] getWeight() {
        return this.weight;
    }
    public int getN(){
        return this.n;
    }

    /** Verifica se a cidade encontra-se no caminho definido por parâmetro. */
    private boolean isCityInPath(int[] path, int city) {
        for (int i = 0; i < path.length; i++) {
            if (path[i] == city) {
                return true;
            }
        }
        return false;
    }    
}