package main;

/** @author messiah */
public class main {
    public static void main(String[] args) throws Exception {
        int line = 0;
        Graph.JGraph jg = null;
        
        try{
            int l = 0; /** Define a linha corrente na leitura do aruivo de entrada. */
            int input = 0; /** Define que o arquivo de leitura comtém, a partir 
            deste instante, a matriz de entrada. */
            int collum = 0; /** Define a coluna do arquivo que será inserida no grafo jg. */
                
            ExtractWord.ExtractWord words = new ExtractWord.ExtractWord(args[0], args[1]);
            String wd = null; /** Define as palavras contidas em uma linha do arquivo. */
            while((wd = words.nextWord()) != null){
                if(wd.compareTo("EDGE_WEIGHT_SECTION") == 0){
                    line = -1;
                    input = 1;
                    wd = words.nextWord();
                }
                /** Define a dimensão do grafo jg. */
                if(wd.compareTo("DIMENSION") == 0){
                    l = words.line;
                    wd = words.nextWord();
                    System.out.println("Matriz com dimensão: " + Integer.parseInt(wd));
                    
                    jg = new Graph.JGraph(Integer.parseInt(wd)); /** Inicializa o grafo de cidades. */                    
                }
                
                /** Incrementa a linha do arquivo de leitura e permite a 
                navegação nas colunas. */                
                if(l != words.line){
                    l = words.line;
                    line++;
                    collum = 0;
                }
                /** Define o inicio da inserção dos elementos no grafo jg. */
                if(input == 1){
                    /** Insere o elemento 'p', da linha 'line' e coluna 'collum' no grafo 'jg'. */
                    jg.insertEdge(line, collum, Integer.parseInt(wd));
                    collum++;
                }
            }
            words.closeFile();            
        }catch (Exception e) {
            System.out.println (e.getMessage ());
        }
                
        /** Inicia a pesquisa pelo vizinho mais próximo. */
        NearestNeighbor.NearestNeighbor nn = new NearestNeighbor.NearestNeighbor(jg.getE(), 0);
        int total_way = 0;
        int[] path = nn.getPath(), weight = nn.getWeight();
        
        for(int i = 0; i < nn.getPath().length; i++){
            total_way += weight[i];
            
            System.out.print(path[i]+"-");
        }
        
        System.out.println("\nDistâcia total: "+total_way);
    }
    
}
