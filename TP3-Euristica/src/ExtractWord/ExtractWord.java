package ExtractWord;


import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.StringTokenizer;

/** @author messiah */
public class ExtractWord {
    private BufferedReader delimiters_file, file;
    private StringTokenizer words;
    private String delimiters;
    public int line;
  
    /** Iniciliza a leitura do arquivo de entrada, a partir de cada linha do mesmo. */
    public ExtractWord(String df, String f) throws FileNotFoundException, IOException{
        this.delimiters_file = new BufferedReader(new FileReader(df));
        this.file = new BufferedReader(new FileReader(f));
        
        this.delimiters = this.delimiters_file.readLine() + "\t:";
        this.words = null;
        this.line = 1;
    }
    /** Possibilita a navegação no arquivo de entrada, acrescentando palavra a 
     palavra, tal que é retornada pelo método. */
    public String nextWord() throws IOException{
        if(this.words == null || !this.words.hasMoreTokens()){
            String l = this.file.readLine();
            this.line++;
            if(l == null) return null;
            this.words = new StringTokenizer(l, this.delimiters);
            if(!this.words.hasMoreTokens()) return "";
        }
        return this.words.nextToken();
    }
    
    /** Fecha os arquivos inicializados no construtor. */
    public void closeFile() throws IOException{
        this.delimiters_file.close();
        this.file.close();
    }
}
