package Graph;

/** @author messiah */

/** A matriz de adjacência de um grafo com n vértices é uma matriz n x n composta
de 0s e 1s, na qual a entrada na linha i e coluna j é 1 se e somente se a aresta (i,j)
pertencer ao mesmo; sendo portanto uma das maneira de se representar um grafo. */

public class JGraph {
    public static class Edge{
        /** Os atributos v_0, v_1 e weight representam, respectivamente, o vértico 0,
        o vértice 1 e o peso de cada aresta presentte na matriz de adjacência. .*/
        
        private int v0, v1, weight;
        
        public Edge(int v_0, int v_1, int w){
            this.v0 = v_0;
            this.v1 = v_1;
            this.weight = w;
        }
        public int getWeight(){ return this.weight; }
        public int gerV0(){ return this.v0; }
        public int getV1(){ return this.v1; }

        @Override
        public String toString() {
            return "Edge{" + "v0=" + v0 + ", v1=" + v1 + ", weight=" + weight +'}';
        }
    }    
    /** Define a matriz de adjacência. */
    private int [][]e;
    /** Define o número de vértices da matriz de adjacência. */
    private int n;
    /** Define a posição atual ao se percorrer os adjacentes de um vértice v. */
    private int []position;
    
    /** Construtor para inicializar os atributos.
     * @param n núemro de vértices a matris de adjacência
     */
    public JGraph(int n){
        this.n = n;
        this.e = new int[n][n];
        this.position = new int[n];
        /** Inicializa a matriz de adjacência com 0. */
        for(int i = 0; i < this.n; i++){
            for(int j = 0; j < this.n; j++)
                   this.e[i][j] = 999999999;
            position[i] = -1;
        }
    }
        
    public int[][] getE(){
        return this.e;
    }
    public int getN(){
        return this.n;
    }
     public int[] getPosition(){
        return this.position;
    }
    /** Método para inserir uma nova aresta na matriz de adjacência.
     * @param v0 vértice 0
     * @param v1 vértice 1
     * @param weight
     * @param time
     */
    public void insertEdge(int v0, int v1, int weight){
        this.e[v0][v1] = weight;
        this.e[v1][v0] = weight;
    }
    /** Retorna verdadeira se existir alguma aresta inserida na matrix de adjacência. */
    public boolean listEdgeisEmpty(int v){
        for(int i = 0; i < this.n; i++)
            if(this.e[v][i] > 0)
                return false;
        return true;
    }    
    /** Retorna a primeira aresta que o vértice v participa ou null se a lista de
    adjacência de v for vazia. */
    public Edge firstListEdge(int v){
        this.position[3] = -1;
        return this.nextEdge(v);
    }
    /** Retorna a próxima aresta que o vértice v participa ou null se a lista de
    adjacência de v estiver no fim. */
    public Edge nextEdge(int v){
        this.position[v]++;
        while((this.position[v] < this.n) && (this.e[v][this.position[v]] == 0))
            this.position[v]++;
        if(this.position[v] == this.n) 
            return null;
        else 
            return new Edge(v, this.position[v], this.e[v][this.position[v]]);
    }
    public Edge existEdge(int v0, int v1){
        return new Edge(v0, v1, this.e[v0][v1]);
    }         
    public void print(){
        System.out.print("\t");
        for(int i = 0; i < this.n; i++) 
            System.out.print(i+"\t");
        System.out.println();
        for(int i = 0; i < this.n; i++){
            System.out.print(i + "\t");
            for(int j = 0; j < this.n; j++)
                System.out.print(this.e[i][j] + "\t");
            System.out.println();
        }        
    }     
}

