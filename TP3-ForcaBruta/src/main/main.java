package main;

import BruteForce.BruteForce;
import Graph.JGraph;
import java.util.Random;

/** @author messiah */
public class main {

    public static void main(String[] args) {
        for(int i=4; i<5; i++){
            System.out.println(i+" cidades inseridas.");
            JGraph jg = new JGraph(i);
            /**Insere os elementos de valores aleátorios no grafo jg. */
            for(int j=0; j<i; j++){
                for(int k=0; k<i; k++){
                    if(j != k)
                        jg.insertEdge(j, k, (new Random()).nextInt(200)+1);
                }
            }
            jg.print();
            /** Inicia a busca por força bruto no garfo jg. */
            BruteForce bf = new BruteForce(jg);
            final long inicial_time = System.currentTimeMillis();
            bf.search();
            final long total_time = (System.currentTimeMillis()-inicial_time);
            bf.display();
            System.out.println("Tempo de pesquisa: "+total_time+"\n");
        }
    }
    
}
